﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace Projekt1_Piekarski50405
{
    /// <summary>
    /// Interaction logic for MpKupionyProduktIReszta.xaml
    /// </summary>
    public partial class MpKupionyProduktIReszta : Window
    {
        public ObservableCollection<MpKupionyProduktIResztaVM> MpPlikiBanknotow { get; set; }
        public string MpSumaReszty { get; set; }
        private MpPojemnikNaPieniadze mpPojemnik;
        private Random mpRand = new Random((int)DateTime.Now.Ticks);

        public MpKupionyProduktIReszta(MpPojemnikNaPieniadze mpPojemnik)
        {
            InitializeComponent();

            DataContext = this;

            this.mpPojemnik = mpPojemnik;

            MpPlikiBanknotow = new ObservableCollection<MpKupionyProduktIResztaVM>();

            foreach (MpPlikBanknotow mpPlik in mpPojemnik.MpPlikiBanknotow)
            {
                MpPlikiBanknotow.Add(new MpKupionyProduktIResztaVM()
                {
                    MpNominal = mpPlik.MpWartoscBanknotu.ToString("0.00")
                    + " " + mpPlik.MpWaluta,
                    MpLiczbaNominalow = mpPlik.MpIloscBanknotow
                });
            }

            MpSumaReszty = mpPojemnik.MpPoliczPieniadze().ToString("0.00");
        }

        private void cvsPojemnikNaReszte_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (MpPlikBanknotow mpPlik in mpPojemnik.MpPlikiBanknotow)
            {
                for (int i = 0; i < mpPlik.MpIloscBanknotow; i++)
                {
                    Button mpBtn = new Button();


                    mpBtn.Background = Brushes.Transparent;
                    mpBtn.BorderBrush = Brushes.Transparent;
                    mpBtn.MaxHeight = 100;
                    mpBtn.Click += MpBtn_Click;
                    mpBtn.Content = new Image() { Source = new BitmapImage(new Uri($"/Obrazy/Banknoty/{mpPlik.MpWaluta}/{mpPlik.MpWartoscBanknotu}.png", UriKind.Relative)) };

                    cvsPojemnikNaReszte.Children.Add(mpBtn);

                    Canvas.SetRight(mpBtn, mpRand.Next(20, (int)cvsPojemnikNaReszte.ActualWidth - 100));
                    Canvas.SetTop(mpBtn, mpRand.Next(20, (int)cvsPojemnikNaReszte.ActualHeight - 100));
                }
            }
        }

        private void MpBtn_Click(object sender, RoutedEventArgs e)
        {
            Button mpBtn = (Button)sender;

            cvsPojemnikNaReszte.Children.Remove(mpBtn);
        }

        public class MpKupionyProduktIResztaVM
        {
            public string MpNominal { get; set; }
            public int MpLiczbaNominalow { get; set; }
        }
    }
}
