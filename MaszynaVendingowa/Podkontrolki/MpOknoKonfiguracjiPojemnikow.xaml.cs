﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace Projekt1_Piekarski50405
{
    /// <summary>
    /// Interaction logic for MpOknoKonfiguracjiPojemnikow.xaml
    /// </summary>
    public partial class MpOknoKonfiguracjiPojemnikow : Window
    {
        public ObservableCollection<MpOknoKonfiguracjiPojemnikowVM> MpRekordy { get; set; } = new ObservableCollection<MpOknoKonfiguracjiPojemnikowVM>();
        public MpPojemnikNaPieniadze[] MpPojemniki { get; set; }

        public MpOknoKonfiguracjiPojemnikow(MpPojemnikNaPieniadze[] mpPojemniki)
        {
            InitializeComponent();

            DataContext = this;

            MpPojemniki = mpPojemniki;

            foreach (MpPojemnikNaPieniadze mpPojemnik in mpPojemniki)
            {
                var mpPanel = new StackPanel() { Margin = new Thickness(20) } as StackPanel;
                mpPanel.Children.Add(new TextBlock()
                {
                    Text = mpPojemnik.MpRodzajWaluty.MpSkrotWaluty,
                    Margin = new Thickness(5)
                });
                mpPanel.Children.Add(new TextBlock()
                {
                    Text = "Nominały:",
                    Margin = new Thickness(5)
                });
                mpPanel.Children.Add(new MpKonfigurowaneNominaly(mpPojemnik.MpPlikiBanknotow));

                var mpPanelChild = new StackPanel() { Orientation = Orientation.Horizontal } as StackPanel;

                mpPanelChild.Children.Add(new TextBlock() { Text = "Suma pieniędzy: ", Margin = new Thickness(5) });
                mpPanelChild.Children.Add(new TextBlock() { Text = mpPojemnik.MpPoliczPieniadze().ToString("0.00"), Margin = new Thickness(5) });

                mpPanel.Children.Add(mpPanelChild);

                Button mpGenerujNominaly = new Button() { Margin = new Thickness(5) };

                mpGenerujNominaly.Tag = mpPojemnik.MpRodzajWaluty.MpSkrotWaluty;
                mpGenerujNominaly.Click += MpGenerujNominaly_Click;
                mpGenerujNominaly.MinHeight = 30;
                mpGenerujNominaly.Content = "Generuj nominały dla " + mpGenerujNominaly.Tag;
                mpGenerujNominaly.Padding = new Thickness(2);

                mpPanel.Children.Add(mpGenerujNominaly);

                stpMain.Children.Add(mpPanel);

            }
        }

        private void MpGenerujNominaly_Click(object sender, RoutedEventArgs e)
        {
            Button mpButton = (Button)sender;

            foreach (MpPojemnikNaPieniadze mpPojemnik in MpPojemniki)
            {
                if (mpPojemnik.MpRodzajWaluty.MpSkrotWaluty == mpButton.Tag.ToString())
                {
                    if (txtMax.Text == "" || txtMin.Text == "")
                    {
                        MessageBox.Show("Brak wpisanych parametrów do generowania nominałów.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    int mpMin, mpMax;
                    try
                    {
                        mpMin = int.Parse(txtMin.Text);
                        mpMax = int.Parse(txtMax.Text);
                    }
                    catch
                    {
                        MessageBox.Show("Nieprawidłowe znaki w liczbach generowanych nominałów.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    if(mpMin < 0 || mpMax < 0)
                    {
                        MessageBox.Show("Liczby generowanych nominałów nie mogą być mniejsze od 0.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    if(mpMax < mpMin)
                    {
                        MessageBox.Show("Liczba maksymalnych generowanych nominałów nie może być mniejsza od liczby minimalnej", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    mpPojemnik.MpGenerujPieniadze(mpMin, mpMax);

                    this.Close();
                }
            }
        }

        public class MpOknoKonfiguracjiPojemnikowVM
        {
            public string MpNazwaWaluty { get; set; }
            public string MpSumaPieniedzy { get; set; }
        }
    }
}
