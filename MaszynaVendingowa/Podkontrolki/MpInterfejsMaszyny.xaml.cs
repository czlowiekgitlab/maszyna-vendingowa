﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;

namespace Projekt1_Piekarski50405
{
    /// <summary>
    /// Interaction logic for MpInterfejsMaszyny.xaml
    /// </summary>
    public partial class MpInterfejsMaszyny : UserControl
    {
        public enum MpTypWyswietlanegoTekstu
        {
            MpPierwszaInstrukcja = 0,
            MpWpisywanyNumerProduktu = 1,
        }

        public MpWyswietlacz mpWyswietlacz;

        private Timer mpZegar;

        public static readonly RoutedEvent MpOkWcisnieteEvent = EventManager.RegisterRoutedEvent("MpOkWcisnieteEvent", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(MpInterfejsMaszyny));

        public static readonly RoutedEvent MpZmienWaluteEvent = EventManager.RegisterRoutedEvent("MpZmienWaluteEvent", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(MpInterfejsMaszyny));

        public event EventHandler MpAnulujWcisniete;

        public MpInterfejsMaszyny()
        {
            InitializeComponent();

            mpZegar = new Timer();

            mpWyswietlacz = new MpWyswietlacz();

            this.DataContext = mpWyswietlacz;

            mpWyswietlacz.MpWyswietlWiadomosc(0);
        }

        public event RoutedEventHandler MpOkWcisniete
        {
            add { AddHandler(MpOkWcisnieteEvent, value); }
            remove { RemoveHandler(MpOkWcisnieteEvent, value); }
        }

        public event RoutedEventHandler MpZmienWalute
        {
            add { AddHandler(MpZmienWaluteEvent, value); }
            remove { RemoveHandler(MpZmienWaluteEvent, value); }
        }

        private void Keypad_Click(object sender, RoutedEventArgs e)
        {
            if (mpWyswietlacz.MpJestWyswietlany[0])
            {
                mpWyswietlacz.MpZmienTypWyswietlanejWiadomosci(1);
                mpWyswietlacz.MpWyswietlanyTekst = "";
            }

            if (mpWyswietlacz.MpWyswietlanyTekst.Length >= 4 || !mpWyswietlacz.MpJestWyswietlany[1])
            {
                return;
            }
            
            Button mpPrzycisk = (Button)sender;

            mpWyswietlacz.MpWyswietlanyTekst += mpPrzycisk.Content.ToString();
        }

        private void Anuluj_Click(object sender, RoutedEventArgs e)
        {
            btnZmienWalute.IsEnabled = true;
            mpZegar.Enabled = false;
            MpAnulujWcisniete(sender, e);
            mpWyswietlacz.MpWyswietlWiadomosc((int)MpTypWyswietlanegoTekstu.MpPierwszaInstrukcja);
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (mpWyswietlacz.MpJestWyswietlany[2])
                return;

            TekstInterfejsuArgs mpArgumenty = new TekstInterfejsuArgs(MpOkWcisnieteEvent, mpWyswietlacz.MpWyswietlanyTekst);

            RaiseEvent(mpArgumenty);

            mpWyswietlacz.MpZmienTypWyswietlanejWiadomosci(2);
            mpWyswietlacz.MpWyswietlanyTekst = mpArgumenty.MpTekstInterfejsu;

            if (!mpArgumenty.MpTekstPrawidlowy)
            {
                mpWyswietlacz.MpZmienTypWyswietlanejWiadomosci(3);
                mpZegar.Interval = 3000;
                mpZegar.Elapsed += MpWrocNaPoczatek;
                mpZegar.Enabled = true;
            }
        }

        private void MpWrocNaPoczatek(Object source, ElapsedEventArgs e)
        {
            mpWyswietlacz.MpWyswietlWiadomosc((int)MpTypWyswietlanegoTekstu.MpPierwszaInstrukcja);
            mpZegar.Enabled = false;
        }

        private void ZmienWalute_Click(object sender, RoutedEventArgs e)
        {
            ZmianaWalutyArgs mpArgumenty = new ZmianaWalutyArgs(MpZmienWaluteEvent, this);

            RaiseEvent(mpArgumenty);

            if (mpWyswietlacz.MpJestWyswietlany[2])
            {
                mpWyswietlacz.MpWyswietlanyTekst = mpWyswietlacz.MpWyswietlanyProdukt.MpNazwaProduktu + "\n"
                    + mpArgumenty.mpAktywnaWalutaPoZmianie.MpPrzeliczZeZlotowek(float.Parse(mpWyswietlacz.MpWyswietlanyProdukt.MpCenaProduktu)).ToString("0.00")
                    + "\n" + mpArgumenty.mpAktywnaWalutaPoZmianie.MpSkrotWaluty;
            }
        }

        public void MpWcisnijAnuluj()
        {
            Anuluj_Click(this, new RoutedEventArgs());
        }
    }

    public class MpWyswietlacz : INotifyPropertyChanged
    {
        private string[] mpWiadomosci = { "Wybierz walutę i wpisz numer produktu",};

        private bool[] mpJestWyswietlany = new bool[10];
        public bool[] MpJestWyswietlany { get => mpJestWyswietlany; }

        private string mpWyswietlanyTekst;
        
        public MpProdukt MpWyswietlanyProdukt { get; set; }

        public string MpWyswietlanyTekst
        {
            get => this.mpWyswietlanyTekst;

            set
            {
                this.mpWyswietlanyTekst= value;
                this.NotifyPropertyChanged("MpWyswietlanyTekst");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string mpNazwaWlasciwosci)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(mpNazwaWlasciwosci));
        }
        
        public void MpWyswietlWiadomosc(int mpNumerWiadomosci)
        {
            MpZmienTypWyswietlanejWiadomosci(mpNumerWiadomosci);

            MpWyswietlanyTekst = mpWiadomosci[mpNumerWiadomosci];
        }

        public void MpZmienTypWyswietlanejWiadomosci(int mpNumerWiadomosci)
        {
            for (int i = 0, iLength = mpJestWyswietlany.Length; i < iLength; i++)
            {
                mpJestWyswietlany[i] = i == mpNumerWiadomosci ? true : false;
            }
        }
    }

    public class TekstInterfejsuArgs : RoutedEventArgs
    {
        public string MpTekstInterfejsu { get; set; }

        public bool MpTekstPrawidlowy { get; set; }

        public TekstInterfejsuArgs(RoutedEvent routedEvent, string mpPrzekazanyTekst) : base(routedEvent)
        {
            this.MpTekstInterfejsu = mpPrzekazanyTekst;
        }
    }

    public class ZmianaWalutyArgs : RoutedEventArgs
    {
        public MpWaluta mpAktywnaWalutaPoZmianie { get; set; }

        public ZmianaWalutyArgs(RoutedEvent routedEvent, object source) : base(routedEvent, source)
        {
        }
    }
}
