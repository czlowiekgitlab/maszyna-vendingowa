﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projekt1_Piekarski50405
{
    /// <summary>
    /// Interaction logic for MpKomoraProduktow.xaml
    /// </summary>
    public partial class MpKomoraProduktow : UserControl
    {
        public MpKomoraProduktow()
        {
            InitializeComponent();

            MpMaksymalnaIloscProduktowDoWyswietlenia = ugridMain.Columns * ugridMain.Rows + 1;
        }

        public ObservableCollection<MpOkienkoProduktu> MpOkienkaProduktow { get; set; }
        private ObservableCollection<MpProdukt> MpListaProduktow { get; set; }
        private int MpMaksymalnaIloscProduktowDoWyswietlenia { get; set; }

        // Wypełnia komorę produktów, MAX to ilość komórek na produkty w gridMain
        public bool MpWypelnijKomoreProduktow(MpProdukt[] mpListaProduktow)
        {
            ugridMain.Children.Clear();

            MpOkienkaProduktow = new ObservableCollection<MpOkienkoProduktu>();
            MpListaProduktow = new ObservableCollection<MpProdukt>();

            for (int i = 0, iLength = mpListaProduktow.Length % MpMaksymalnaIloscProduktowDoWyswietlenia;
                i < iLength; i++)
            {
                MpOkienkaProduktow.Add(new MpOkienkoProduktu()
                {
                    NumerProduktu = mpListaProduktow[i].MpNumerProduktu,
                    NazwaProduktu = mpListaProduktow[i].MpNazwaProduktu,
                });

                MpOkienkaProduktow[i].MpWlasciwosci.CenaProduktu = mpListaProduktow[i].MpCenaProduktu + " PLN";

                MpListaProduktow.Add(mpListaProduktow[i]);

                ugridMain.Children.Add(MpOkienkaProduktow[i]);
            }

            return true;
        }

        public bool MpWyswietlCeneWWalucie(MpWaluta mpWaluta)
        {
            for (int i = 0, iLength = MpOkienkaProduktow.Count; i < iLength; i++)
            {
                MpOkienkaProduktow[i].MpWlasciwosci.CenaProduktu = mpWaluta.MpPrzeliczZeZlotowek(float.Parse(MpListaProduktow[i].MpCenaProduktu)).ToString("0.00")
                    + $" {mpWaluta.MpSkrotWaluty}";
            }

            return true;
        }
    }
}
