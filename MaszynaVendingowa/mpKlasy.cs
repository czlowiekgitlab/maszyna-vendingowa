﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Piekarski50405
{
    public class MpWaluta
    {
        private string mpSkrotWaluty;
        private float[] mpWartosciNominalow;
        private float mpWartoscZlotowki;
        
        // Properties Read Only
        public string MpSkrotWaluty { get => this.mpSkrotWaluty; }

        // Properties Read-Write
        public float[] MpWartosciNominalow
        {
            get => this.mpWartosciNominalow;

            set
            {
                // Nominały nie mogą wynosić 0 lub mniej
                for (int i = 0, iLength = value.Length; i < iLength; i++)
                {
                    if((value[i] <= 0))
                        throw new Exception("Błąd przy tworzeniu waluty:\n\n" +
                            "Wartości nominałów waluty nie mogą wynosić lub być mniejsze niż 0");
                }

                this.mpWartosciNominalow = new float[value.Length];

                value.CopyTo(this.mpWartosciNominalow, 0);
            }
        }

        public float MpWartoscZlotowki
        {
            get => this.mpWartoscZlotowki;

            set
            {
                mpWartoscZlotowki = (value < 0) ? 0 : value;
            }
        }

        // Metody

        public bool MpSprawdzCzyNominalNalezyDoWaluty(float mpNominal)
        {
            bool mpNominalNalezyDoWaluty = false;
            for (int i = 0, iLength = this.mpWartosciNominalow.Length; i < iLength; i++)
            {
                if (mpNominal == this.mpWartosciNominalow[i])
                {
                    mpNominalNalezyDoWaluty = true;
                    break;
                }
            }

            return mpNominalNalezyDoWaluty;
        }

        public float MpPrzeliczZeZlotowek(float mpKwotaWZlotowkach)
        {
            decimal mpPrzeliczonaKwota = (decimal)mpKwotaWZlotowkach * (decimal)MpWartoscZlotowki;

            mpPrzeliczonaKwota = Math.Round(mpPrzeliczonaKwota, 2);

            return (float)mpPrzeliczonaKwota;
        }

        // Konstruktor tworzy walutę o podanej nazwie i określonych wartościach nominałów banknotów
        public MpWaluta(string mpSkrotWaluty, float[] mpWartosciNominalow, float mpWartoscZlotowki)
        {
            this.mpSkrotWaluty = mpSkrotWaluty;

            this.MpWartosciNominalow = mpWartosciNominalow;

            this.MpWartoscZlotowki = mpWartoscZlotowki;
        }
    }

    public class MpPlikBanknotow
    {
        private MpWaluta mpRodzajWaluty;
        private int mpIloscBanknotow;
        private float mpWartoscBanknotu;

        // Properties Read Only
        public float MpWartoscBanknotu { get => this.mpWartoscBanknotu; }

        public string MpWaluta { get => this.mpRodzajWaluty.MpSkrotWaluty; }

        // Properties Read-Write
        public int MpIloscBanknotow
        {
            get => this.mpIloscBanknotow;

            // Ilość banknotów nie może być mniejsza niż 0
            set => this.mpIloscBanknotow = value >= 0 ? value : 0;
        }

        // Konstruktor tworzy plik o określonej walucie, wartości banknotu i ilości banknotów
        public MpPlikBanknotow (ref MpWaluta mpRodzajWaluty, float mpWartoscBanknotu, int mpIloscBanknotow)
        {
            // Wyświetl błąd jeżeli nominał nie istnieje
            if(!mpRodzajWaluty.MpSprawdzCzyNominalNalezyDoWaluty(mpWartoscBanknotu))
                throw new Exception($"Błąd przy tworzeniu pliku banknotów:\n\n" +
                    $"Podany nominał {mpWartoscBanknotu.ToString()} nie istnieje w walucie {mpRodzajWaluty.MpSkrotWaluty}");

            // Referencja do istniejącego obiektu określającego walutę
            this.mpRodzajWaluty = mpRodzajWaluty;

            this.mpWartoscBanknotu = mpWartoscBanknotu;

            this.MpIloscBanknotow = mpIloscBanknotow;
        }
    }

    public class MpPojemnikNaPieniadze : IPojemnikNaPieniadze
    {
        private MpPlikBanknotow[] mpPlikiBanknotow;
        private MpWaluta mpRodzajWaluty;

        // Properties Read Only
        public MpWaluta MpRodzajWaluty { get => mpRodzajWaluty; }

        public MpPlikBanknotow[] MpPlikiBanknotow
        {
            get => this.mpPlikiBanknotow;
        }

        // Metody
        // Sprawdza czy Min i Max są zgodne
        private void MpSprawdzMinMaxLiczbeBanknotow(int mpMinLiczbaBanknotow, int mpMaxLiczbaBanknotow)
        {
            string mpKomunikatBledu = "";

            if (mpMinLiczbaBanknotow < 0 || mpMaxLiczbaBanknotow < 0)
                mpKomunikatBledu += "Maksymalna i minimalna liczba wylosowanych banknotów nie może być mniejsza od 0\n\n";

            if (mpMaxLiczbaBanknotow < mpMinLiczbaBanknotow)
                mpKomunikatBledu += "Maksymalna liczba banknotów nie może być mniejsza niż minimalna liczba banknotów";

            if (mpKomunikatBledu != "")
                throw new Exception("Błąd przy losowym generowaniu banknotów:\n\n" +
                    mpKomunikatBledu);
        }

        /* Generuje losowe ilości banknotów dla każdego pliku banknotów
        Znajdującego się w pojemniku */
        public void MpGenerujPieniadze(int mpMinLiczbaBanknotow, int mpMaxLiczbaBanknotow)
        {
            MpSprawdzMinMaxLiczbeBanknotow(mpMinLiczbaBanknotow, mpMaxLiczbaBanknotow);

            Random mpRandom = new Random();

            for (int i = 0, iLength = this.mpPlikiBanknotow.Length; i < iLength; i++)
            {
                this.mpPlikiBanknotow[i].MpIloscBanknotow = mpRandom.Next(mpMinLiczbaBanknotow, mpMaxLiczbaBanknotow);
            }
        }
        
        public bool MpWplacBanknot(float mpBanknot)
        {
            for (int i = 0, iLength = MpPlikiBanknotow.Length; i < iLength; i++)
            {
                if (MpPlikiBanknotow[i].MpWartoscBanknotu == mpBanknot)
                {
                    MpPlikiBanknotow[i].MpIloscBanknotow++;

                    return true;
                }
            }

            return false;
        }

        public float MpPoliczPieniadze()
        {
            float mpSumaPieniedzy = 0;

            foreach (MpPlikBanknotow mpPlik in this.mpPlikiBanknotow)
            {
                mpSumaPieniedzy += mpPlik.MpIloscBanknotow * mpPlik.MpWartoscBanknotu;
            }

            return mpSumaPieniedzy;
        }

        public MpPojemnikNaPieniadze MpWyplacReszte(float mpReszta)
        {
            decimal mpWyplaconaReszta = (decimal)mpReszta;
            MpPojemnikNaPieniadze mpPojemnikNaReszte = new MpPojemnikNaPieniadze(ref this.mpRodzajWaluty);

            for (int i = 0, iLength = this.mpPlikiBanknotow.Length; i < iLength; i++)
            {
                float mpWartoscBanknotu = MpPlikiBanknotow[i].MpWartoscBanknotu;
                int mpIloscBanknotow = MpPlikiBanknotow[i].MpIloscBanknotow;

                while ((decimal)mpWartoscBanknotu <= mpWyplaconaReszta && mpIloscBanknotow != 0)
                {
                    mpWyplaconaReszta -= (decimal)mpWartoscBanknotu;
                    MpPlikiBanknotow[i].MpIloscBanknotow--;

                    mpPojemnikNaReszte.MpWplacBanknot(mpWartoscBanknotu);
                }
            }
            
            return mpPojemnikNaReszte;
        }
        
        // Konstruktory
        public MpPojemnikNaPieniadze(ref MpWaluta mpRodzajWaluty, int mpMinLiczbaBanknotow, int mpMaxLiczbaBanknotow)
        {
            this.mpPlikiBanknotow = new MpPlikBanknotow[mpRodzajWaluty.MpWartosciNominalow.Length];

            MpSprawdzMinMaxLiczbeBanknotow(mpMinLiczbaBanknotow, mpMaxLiczbaBanknotow);

            Random mpRandom = new Random();

            for (int i = 0, iLength = mpPlikiBanknotow.Length; i < iLength; i++)
            {
                mpPlikiBanknotow[i] = new MpPlikBanknotow(ref mpRodzajWaluty, mpRodzajWaluty.MpWartosciNominalow[i],
                    mpRandom.Next(mpMinLiczbaBanknotow, mpMaxLiczbaBanknotow));
            }

            this.mpRodzajWaluty = mpRodzajWaluty;
        }

        public MpPojemnikNaPieniadze(ref MpWaluta mpRodzajWaluty) : this(ref mpRodzajWaluty, 0, 0)
        {

        }
    }

    public class MpProdukt : IProdukt
    {
        static int mpNastepnyNumerProduktu = 0;
        float mpCenaProduktuFloat;

        public int MpNumerProduktu { get; }
        public string MpCenaProduktu
        {
            get => mpCenaProduktuFloat.ToString("0.00");
        }
        public string MpNazwaProduktu { get; }

        public override string ToString()
        {
            return "Numer Produktu: " + MpNumerProduktu
                + "\nNazwa Produktu: " + MpNazwaProduktu
                + "\nCena Produktu: " + MpCenaProduktu;
        }

        // Numer każdego następnego produktu jest ustalany od 0
        public MpProdukt(string mpNazwaProduktu, float mpCenaProduktu)
        {
            string mpKomunikatBledu = "";

            //if (mpNumerProduktu < 0)
            //    mpKomunikatBledu += "Numer produktu nie może być mniejszy niż 0\n\n";

            if (mpCenaProduktu < 0)
                mpKomunikatBledu += "Cena produktu nie może być mniejsza niż 0\n\n";

            if (mpNazwaProduktu == "")
                mpKomunikatBledu += "Nazwa produktu nie może być pusta";

            if (mpKomunikatBledu != "")
                throw new Exception("Błąd tworzeniu produktu:\n\n" +
                    mpKomunikatBledu);

            this.MpNumerProduktu = mpNastepnyNumerProduktu;
            this.MpNazwaProduktu = mpNazwaProduktu;
            this.mpCenaProduktuFloat = mpCenaProduktu;
            mpNastepnyNumerProduktu++;
        }
    }
}
